<?php
/**
 * @file
 * This is the code for deleting accounts.
 */

/**
 * Delete a mailbox account.
 *
 * @param object $account
 *   The user object on which the operation is being performed.
 */
function _couriermta_delete_mailbox_account($account) {
  $query = 'SELECT maildir FROM {couriermta_users} WHERE userid = :userid';
  $mailboxdir = db_query($query, array(':userid' => $account->uid))->fetchField();
  module_load_include('inc', 'couriermta', 'couriermta.common');
  if ($mailboxdir) {
    // Update the deletemailbox file.
    _couriermta_update_input_file('delete', 'mailbox', $mailboxdir . '
');
  }
  else {
    // Update the deletemailbox file.
    $query = 'SELECT home FROM {couriermta_users} WHERE userid = :userid';
    $results = db_query($query, array(':userid' => $account->uid));
    $data = $results->fetchField() . '
';
    _couriermta_update_input_file('delete', 'mailbox', $data);
  }

  // Update the couriermta_users table.
  $results = db_delete('couriermta_users')
    ->condition('userid', $account->uid)
    ->condition('email', $account->mail)
    ->execute();
  if (!$results) {
    $message = 'The query to remove data from the couriermta_users table was not successfully executed.';
    watchdog('couriermta', $message, array(), WATCHDOG_ERROR);
  }
  else {
    $message = 'Deleting data: accountid="%d", and email address="%s".';
    watchdog('couriermta', $message, array('%d' => $account->uid, '%s' => $account->mail));
  }
}


/**
 * Delete an alias account.
 *
 * @param object $account
 *   The user object on which the operation is being performed.
 */
function _couriermta_delete_aliased_account(&$account) {
  // Update the deletealias file.
  $query = 'SELECT alias FROM {couriermta_users} WHERE userid = :userid';
  $results = db_query($query, array(':userid' => $account->uid));
  $data = $account->mail . ': ' . $results->fetchField() . '
';
  module_load_include('inc', 'couriermta', 'couriermta.common');
  _couriermta_update_input_file('delete', 'alias', $data);

  // Update the couriermta_users table.
  $result = db_delete('couriermta_users')
    ->condition('userid', $account->uid)
    ->execute();
  if (!$result) {
    $message = 'The query to remove data from the couriermta_users table was not successfully executed.';
    watchdog('couriermta', $message, array(), WATCHDOG_ERROR);
  }
}
