#!/bin/bash

##
##  This script is for the execution of mailbox and alias changes created
##    through the Courier-MTA module. This script should be executed at
##    regular intervals through a cronjob.
##
##  Installation:
##    * Edit the variable settings so that they match your system.
##    * Create a cron job as root that executes this script at regular
##      intervals (i.e.: every 15 minutes).
##

###############################
##  Variable initialization  ##
###############################

##  Change the variables in this section to match your system's settings.

# This is the location where the Courier-MTA module is installed. This script
# will look there for the information on which mailboxes and aliases to
# insert and delete.
COURIERMTA_MODULE_LOCATION=/var/www/drupal5/sites/localhost/modules/couriermta

# These are the variables that define what is necessary to insert and delete
# mailboxes.
#MAILBOX_LOCATION=/var/spool/mail
MAILBOX_USER=virtual
MAILBOX_GROUP=virtual
MAILDIRMAKE_COMMAND=/usr/bin/maildirmake

# These are the variables that define what is necessary to insert and delete
# aliases.
ALIAS_FILE=/etc/courier/aliases/drupal
MAKEALIASES_COMMAND=/usr/sbin/makealiases

# A temporary working file.
TEMP_WORKING_FILE=/tmp/couriermtaworking.txt

###################
##  Main script  ##
###################

## Delete alias
# Make sure the input file exists.
if [ -e ${COURIERMTA_MODULE_LOCATION}/deletealias ]
then
  # Read the input file.
  cat ${COURIERMTA_MODULE_LOCATION}/deletealias | while read LINE
  do
    # Remove the alias.
    COMMAND_STR="sed '/^${LINE}$/d' ${ALIAS_FILE} > ${TEMP_WORKING_FILE}"
    eval ${COMMAND_STR}
    # Move the working file to the aliases file.
    mv ${TEMP_WORKING_FILE} ${ALIAS_FILE}
  done
  # Reset the input file to avoid unwanted side effects.
  rm ${COURIERMTA_MODULE_LOCATION}/deletealias
  # Run the makealiases command.
  ${MAKEALIASES_COMMAND}
fi


## Insert alias
# Make sure the input file exists.
if [ -e ${COURIERMTA_MODULE_LOCATION}/insertalias ]
then
  # Append the input file to the aliases file.
  cat ${COURIERMTA_MODULE_LOCATION}/insertalias >> ${ALIAS_FILE}
  # Reset the input file to avoid unwanted side effects.
  rm ${COURIERMTA_MODULE_LOCATION}/insertalias
  # Run the makealiases command.
  ${MAKEALIASES_COMMAND}
fi


## Insert mailbox
# Make sure the input file exists.
if [ -e ${COURIERMTA_MODULE_LOCATION}/insertmailbox ]
then
  # Read the input file.
  cat ${COURIERMTA_MODULE_LOCATION}/insertmailbox | while read LINE
  do
    # Create the parent directories, if missing.
    if [ ! -d `dirname ${LINE}` ]
    then
      mkdir -p `dirname ${LINE}`
      chown -R ${MAILBOX_USER}\:${MAILBOX_GROUP} `dirname ${LINE}`
      chmod -R ug+rwx `dirname ${LINE}`
    fi

    #Only create the directory if it doesn't already exist.
    if [ ! -e ${LINE} ]
    then
      # Create the new mailbox.
      ${MAILDIRMAKE_COMMAND} ${LINE}
      # Change the ownership and permissions so that e-mail opperates correctly.
      chown -R ${MAILBOX_USER}\:${MAILBOX_GROUP} ${LINE}
      chmod -R ug+rwx ${LINE}
    fi
  done
  # Reset the input file to avoid unwanted side effects.
  rm ${COURIERMTA_MODULE_LOCATION}/insertmailbox
fi


## Update mailbox
# Make sure the input file exists.
if [ -e ${COURIERMTA_MODULE_LOCATION}/updatemailbox ]
then
  # Read the input file.
  cat ${COURIERMTA_MODULE_LOCATION}/updatemailbox | while read LINE
  do
    # Extract the the old mailbox name from the input line.
    OLDMAILBOX=${LINE%*:*}
    # Extract the the new mailbox name from the input line.
    NEWMAILBOX=${LINE#*:*}
    # Make sure the old mailbox exists and the new one does not.
    if [ -d ${OLDMAILBOX} ] && [ ! -e ${NEWMAILBOX} ]
    then
      # Move the mailbox from the old name to the new one.
      mv ${OLDMAILBOX} ${NEWMAILBOX}
    fi
  done
  # Reset the input file to avoid unwanted side effects.
  rm ${COURIERMTA_MODULE_LOCATION}/updatemailbox
fi


## Delete mailbox
# Make sure the input file exists.
if [ -e ${COURIERMTA_MODULE_LOCATION}/deletemailbox ]
then
  # Read the input file.
  cat ${COURIERMTA_MODULE_LOCATION}/deletemailbox | while read LINE
  do
    # Make sure the mailbox exists.
    if [ -e ${LINE} ]
    then
      # Delete the old mailbox.
      rm -rf ${LINE}
    fi
  done
  # Reset the input file to avoid unwanted side effects.
  rm ${COURIERMTA_MODULE_LOCATION}/deletemailbox
fi
