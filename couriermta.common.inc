<?php
/**
 * @file
 * This is some common functionality that is used in multiple files.
 */

/**
 * Create an aggregated user data array.
 *
 * This function takes user information from multiple sources and combines it
 * into a single array that can be used to update the couriermta_users table.
 *
 * @param array $edit
 *   The form values submitted by the user.
 * @param object $account
 *   The user object on which the operation is being performed.
 *
 * @return array
 *   The user's aggregated data.
 */
function _couriermta_create_user_sql_data(&$edit, &$account) {
  $user_data = array();
  $user_data['userid'] = $account->uid;

  if (module_exists('token') && variable_get('couriermta_use_token_module', 0) == 1) {
    if (variable_get('couriermta_email_users_address', 1) == 1) {
      $user_data['email'] = $edit['mail'];
    }
    else {
      $user_data['email'] = token_replace(variable_get('couriermta_email_pattern', ''));
    }

    if (variable_get('couriermta_password_users_password', 1) == 1) {
      $user_data['password'] = $edit['couriermta_passsword'];
    }
    else {
      $user_data['password'] = token_replace(variable_get('couriermta_password_encrypted', ''));
    }

    if (variable_get('couriermta_users_name', 1) == 1) {
      $user_data['name'] = $edit['name'];
    }
    else {
      $user_data['name'] = token_replace(variable_get('couriermta_name', ''));
    }

    $user_data['uid'] = token_replace(variable_get('couriermta_uid_pattern', '65534'));
    $user_data['gid'] = token_replace(variable_get('couriermta_gid_pattern', '65534'));

    $user_data['home']    = token_replace(variable_get('couriermta_home_pattern', '/var/spool/mail/'));
    $user_data['maildir'] = token_replace(variable_get('couriermta_maildir_pattern', '/var/spool/mail/'));

    $user_data['defaultdelivery'] = token_replace(variable_get('couriermta_defaultdelivery_pattern', ''));
    $user_data['quota']           = token_replace(variable_get('couriermta_quota_pattern', ''));
    $user_data['auxoptions']      = token_replace(variable_get('couriermta_options_pattern', ''));
  }
  else {
    if (variable_get('couriermta_email_users_address', 1) == 1) {
      $user_data['email'] = $edit['mail'];
    }
    else {
      $user_data['email'] = variable_get('couriermta_email_pattern', '');
    }

    if (variable_get('couriermta_password_users_password', 1) == 1) {
      $user_data['password'] = $edit['couriermta_passsword'];
    }
    else {
      $user_data['password'] = variable_get('couriermta_password_encrypted', '');
    }

    if (variable_get('couriermta_users_name', 1) == 1) {
      if (!empty($edit['name'])) {
        $user_data['name'] = $edit['name'];
      }
      else {
        $user_data['name'] = $account->name;
      }
    }
    else {
      $user_data['name'] = variable_get('couriermta_name', '');
    }

    $user_data['uid'] = variable_get('couriermta_uid_pattern', '65534');
    $user_data['gid'] = variable_get('couriermta_gid_pattern', '65534');

    $user_data_home = variable_get('couriermta_home_pattern', '/var/spool/mail/!domain_name/!user_name');
    if (strstr($user_data_home, '!domain_name')) {
      if (strstr($user_data_home, '!user_name')) {
        $user_data_home = str_replace('!user_name', drupal_substr($edit['mail'], 0, strpos($edit['mail'], '@')), $user_data_home);
        $user_data_home = str_replace('!domain_name', drupal_substr($edit['mail'], strpos($edit['mail'], '@') + 1, drupal_strlen($edit['mail'])), $user_data_home);
      }
      else {
        $user_data_home = str_replace('!domain_name', drupal_substr($edit['mail'], strpos($edit['mail'], '@') + 1, drupal_strlen($edit['mail'])), $user_data_home);
      }
    }
    else {
      if (strstr($user_data_home, '!user_name')) {
        $user_data_home = str_replace('!user_name', drupal_substr($edit['mail'], 0, strpos($edit['mail'], '@')), $user_data_home);
      }
      else {
      }
    }
    $user_data['home'] = $user_data_home;

    $user_data_maildir = variable_get('couriermta_maildir_pattern', '/var/spool/mail/!domain_name/!user_name');
    if (strstr($user_data_maildir, '!domain_name')) {
      if (strstr($user_data_maildir, '!user_name')) {
        $user_data_maildir = str_replace('!user_name', drupal_substr($edit['mail'], 0, strpos($edit['mail'], '@')), $user_data_maildir);
        $user_data_maildir = str_replace('!domain_name', drupal_substr($edit['mail'], strpos($edit['mail'], '@') + 1, drupal_strlen($edit['mail'])), $user_data_maildir);
      }
      else {
        $user_data_maildir = str_replace('!domain_name', drupal_substr($edit['mail'], strpos($edit['mail'], '@') + 1, drupal_strlen($edit['mail'])), $user_data_maildir);
      }
    }
    else {
      if (strstr($user_data_maildir, '!user_name')) {
        $user_data_maildir = str_replace('!user_name', drupal_substr($edit['mail'], 0, strpos($edit['mail'], '@')), $user_data_maildir);
      }
    }
    $user_data['maildir'] = $user_data_maildir;

    $user_data['defaultdelivery'] = variable_get('couriermta_defaultdelivery_pattern', '');
    $user_data['quota'] = variable_get('couriermta_quota_pattern', '');
    $user_data['auxoptions'] = variable_get('couriermta_options_pattern', '');
  }

  if (variable_get('couriermta_allow_email_alias', 0) == 1) {
    $user_data['alias'] = $edit['couriermta_email_alias_address'];
  }

  if (!empty($edit['status'])) {
    $user_data['status'] = $edit['status'];
  }
  else {
    $user_data['status'] = $account->status;
  }

  return $user_data;
}


/**
 * Update the relevant input file.
 *
 * @param string $operation
 *   The type of operation being performed. Valid operations are insert, update,
 *   and delete.
 * @param string $email_type
 *   The type of e-mail account. Valid types are mailbox and alias.
 * @param string $file_contents
 *   The contents to be written to the file.
 */
function _couriermta_update_input_file($operation, $email_type, $file_contents) {
  $file_name = drupal_get_path('module', 'couriermta') . '/' . $operation . $email_type;
  $file_handle = fopen($file_name, 'a');

  if (!$file_handle) {
    $message = 'The %operation %email_type file could not be opened. Check to make sure valid permissions are in place for %file_name.';
    watchdog(
      'couriermta',
      $message,
      array(
        '%operation' => $operation,
        '%email_type' => $email_type,
        '%file_name' => $file_name),
      WATCHDOG_ERROR);
  }
  else {
    if (!fwrite($file_handle, $file_contents)) {
      watchdog('couriermta', 'There was an error writing to the alias file.', array(), WATCHDOG_ERROR);
    }
    else {
      watchdog('couriermta', 'Successfully wrote to %file_name.', array('%file_name' => $file_name));
    }

    if (!fclose($file_handle)) {
      watchdog('couriermta', 'There was an error closing the alias file.', array(), WATCHDOG_ERROR);
    }
  }
}
