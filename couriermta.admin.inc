<?php
/**
 * @file
 * This is where the admin settings page is created.
 */

/**
 * Courier user administration settings form.
 *
 * @return array
 *   The admin settings form.
 */
function couriermta_admin_settings($form, &$form_state) {
  $form = array();

  $form['couriermta_instructions_line1'] = array(
    '#value' => t('For each of the pattern fields below, if you specify an absolute value, that value will be saved in the database for each user as specified in the form field. If you enter a pattern, that pattern will be modified on a per user basis and saved in the database that way.'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );
  $form['couriermta_instructions_line2'] = array(
    '#value' => t("For example, if you entered '/var/spool/mail' for the home form field below, that would be the value that would be saved in the database for each user. If, however, you entered '/var/spool/mail/!domain_name/!user_name' for the home form field below, the value that would be saved in the database would be based on the users' e-mail address and would be different for every user."),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );

  if (module_exists('token')) {
    $form['couriermta_token'] = array(
      '#type' => 'fieldset',
      '#title' => t('Token parsing'),
      '#collapsible' => TRUE,
      '#description' => t("If you have the Token module installed, you may use that module's tokens for any of the pattern fields below. Posible replcement tokens are listed in the 'Replacement tokens' fieldset below."),
    );
    $form['couriermta_token']['couriermta_use_token_module'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use Token module'),
      '#default_value' => variable_get('couriermta_use_token_module', 0),
      '#description' => t("Check this box if you wish to use the Token module's parsing for the pattern fields below."),
    );
    $form['couriermta_token']['couriermta_replacement_tokens'] = array(
      '#type' => 'fieldset',
      '#title' => t('Replacement tokens'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('You may use any of the following replacements tokens for use in any of the pattern fields below.'),
    );
    // TODO Please change this theme call to use an associative array for the
    // $variables parameter.
    $form['couriermta_token']['couriermta_replacement_tokens']['token_help'] = array(
      '#value' => theme('token_help', 'global'),
    );
  }
  else {
    $form['couriermta_token'] = array(
      '#type' => 'fieldset',
      '#title' => t('Token parsing'),
      '#collapsible' => TRUE,
      '#description' => t("If you have the Token module installed, you may use that module's tokens for any of the pattern fields below."),
    );
    $form['couriermta_token']['couriermta_use_token_module'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use Token module'),
      '#disabled' => TRUE,
      '#default_value' => variable_get('couriermta_use_token_module', 0),
      '#description' => t('Install and enable the Token module to enable this field.'),
    );
  }

  $form['couriermta_email'] = array(
    '#type' => 'fieldset',
    '#title' => t('E-mail address'),
    '#collapsible' => TRUE,
    '#description' => t("Use this area to specify how the user's Courier e-mail address should be entered in the Courier database table."),
  );
  $form['couriermta_email']['couriermta_email_users_address'] = array(
    '#type' => 'checkbox',
    '#title' => t("Use the user's given e-mail address"),
    '#default_value' => variable_get('couriermta_email_users_address', 1),
    '#description' => t("If this is checked, this field will be filled in with the user's Drupal E-mail address."),
  );
  $form['couriermta_email']['couriermta_email_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail pattern'),
    '#default_value' => variable_get('couriermta_email_pattern', ''),
    '#description' => t("The default value for the e-mail field. This field will be ignored if the 'Use the user's given e-mail address' checkbox above is checked."),
  );

  $form['couriermta_password'] = array(
    '#type' => 'fieldset',
    '#title' => t('Password'),
    '#collapsible' => TRUE,
    '#description' => t("Use this area to specify how the user's Courier password should be entered in the Courier database table."),
  );
  $form['couriermta_password']['couriermta_password_type'] = array(
    '#type' => 'radios',
    '#title' => t('Password type to create'),
    '#default_value' => variable_get('couriermta_password_type', 0),
    '#options' => array(t('Encrypted'), t('Cleartext'), t('Both')),
    '#description' => t('Choose whether to create an encrypted password, a cleartext password or both.'),
  );
  $form['couriermta_password']['couriermta_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password pattern'),
    '#default_value' => variable_get('couriermta_password', ''),
    '#description' => t("The default value for the password field."),
  );

  $form['couriermta_name'] = array(
    '#type' => 'fieldset',
    '#title' => t('User name'),
    '#collapsible' => TRUE,
    '#description' => t("Use this area to specify how the user's Courier user's name field should be entered in the Courier database table."),
  );
  $form['couriermta_name']['couriermta_users_name'] = array(
    '#type' => 'checkbox',
    '#title' => t("Use the user's given user name"),
    '#default_value' => variable_get('couriermta_users_name', 1),
    '#description' => t("If this is checked, this field will be filled in with the user's Drupal Username."),
  );
  $form['couriermta_name']['couriermta_name_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Name pattern'),
    '#default_value' => variable_get('couriermta_name', ''),
    '#description' => t("The default value for the name field. This field will be ignored if the 'Use the user's given user name.' checkbox above is checked."),
  );

  $form['couriermta_uid'] = array(
    '#type' => 'fieldset',
    '#title' => t('UID'),
    '#collapsible' => TRUE,
    '#description' => t("Use this area to specify how the user's Courier UID field should be entered in the Courier database table."),
  );
  $form['couriermta_uid']['couriermta_uid_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('UID pattern'),
    '#default_value' => variable_get('couriermta_uid_pattern', '65534'),
    '#description' => t('The default value for the user id field.'),
  );

  $form['couriermta_gid'] = array(
    '#type' => 'fieldset',
    '#title' => t('GID'),
    '#collapsible' => TRUE,
    '#description' => t("Use this area to specify how the user's Courier GID field should be entered in the Courier database table."),
  );
  $form['couriermta_gid']['couriermta_gid_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('GID pattern'),
    '#default_value' => variable_get('couriermta_gid_pattern', '65534'),
    '#description' => t('The default value for the group id field.'),
  );

  $form['couriermta_home'] = array(
    '#type' => 'fieldset',
    '#title' => t('Home'),
    '#collapsible' => TRUE,
    '#description' => t("Use this area to specify how the user's Courier home field should be entered in the Courier database table."),
  );
  $form['couriermta_home']['couriermta_home_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Home pattern'),
    '#default_value' => variable_get('couriermta_home_pattern', '/var/spool/mail/!domain_name/!user_name'),
    '#description' => t("The default value for the home field. You may use '!domain_name' to represent the domain part of the user's e-mail address (the part after the @ sign) and '!user_name' to represent the user part of the user's e-mail address (the part before the @ sign). If you have checked the box above to use the Token module for pattern field parsing, !domain_name and !user_name will not work."),
  );

  $form['couriermta_maildir'] = array(
    '#type' => 'fieldset',
    '#title' => t('Maildir'),
    '#collapsible' => TRUE,
    '#description' => t("Use this area to specify how the user's Courier maildir field should be entered in the Courier database table."),
  );
  $form['couriermta_maildir']['couriermta_maildir_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Maildir pattern'),
    '#default_value' => variable_get('couriermta_maildir_pattern', '/var/spool/mail/!domain_name/!user_name'),
    '#description' => t("The default value for the home field. You may use '!domain_name' to represent the domain part of the user's e-mail address (the part after the @ sign) and '!user_name' to represent the user part of the user's e-mail address (the part before the @ sign). If you have checked the box above to use the Token module for pattern field parsing, !domain_name and !user_name will not work."),
  );

  $form['couriermta_defaultdelivery'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default delivery'),
    '#collapsible' => TRUE,
    '#description' => t("Use this area to specify how the user's Courier defaultdelivery field should be entered in the Courier database table."),
  );
  $form['couriermta_defaultdelivery']['couriermta_defaultdelivery_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Default delivery pattern'),
    '#default_value' => variable_get('couriermta_defaultdelivery_pattern', ''),
    '#description' => t('The default value for the default delivery field.'),
  );

  $form['couriermta_quota'] = array(
    '#type' => 'fieldset',
    '#title' => t('Quota'),
    '#collapsible' => TRUE,
    '#description' => t("Use this area to specify how the user's Courier quota field should be entered in the Courier database table."),
  );
  $form['couriermta_quota']['couriermta_quota_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Quota pattern'),
    '#default_value' => variable_get('couriermta_quota_pattern', ''),
    '#description' => t('The default value for the quota field.'),
  );

  $form['couriermta_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Options'),
    '#collapsible' => TRUE,
    '#description' => t("Use this area to specify how the user's Courier options field should be entered in the Courier database table."),
  );
  $form['couriermta_options']['couriermta_options_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Options pattern'),
    '#default_value' => variable_get('couriermta_options_pattern', ''),
    '#description' => t('The default value for the options field.'),
  );

  $form['couriermta_allow_email_alias'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow users to specify an e-mail alias'),
    '#default_value' => variable_get('couriermta_allow_email_alias', 0),
    '#description' => t('Check this option if you want to allow the users to specify an e-mail address to forward e-mails to.'),
  );

  return system_settings_form($form);
}
