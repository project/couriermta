ABOUT
-----
The Courier MTA module gives administrators and/or users the ability of
managing e-mail accounts on a Courier mail server through a Druapl website.

There is the possibility that this module will do more in the future, but for
now it is simply a user account management module.


AUTHOR
------
Jason Flatt
drupal@oadaeh.net
http://drupal.org/user/4649


INSTALLATION AND CONFIGURATION
------------------------------
In order to use this module effectively, you will need to do a few things.
 * Install the module in the usual way.
 * Configure the settings on the admin/settings/couriermta page. There are
   reasonable defaults in place, in case you've setup your server in a
   conventional manor, but you should certainly change them to match your
   existing settings.
 * Change your authmysqlrc file in your Courier installation to point to the
   couriermta table in your Drupal database. A sample configuration is
   included below for your comparison.
 * Configure the included couriermta.sh shell script to fit your system.
 * Setup a cron job to run the couriermta.sh script at a regular interval. A
   15 to 30 minute interval should be fine, unless you have a busy site with
   lots of user changes, then you might want a shorter interval.


Some sample settings for a /etc/courier/authmysqlrc file:
---------------------------------------------------------
I've configured the database so that it closely matches the default file
configuration. If you leave everything as is, you should only need to specify
the MYSQL_USERNAME, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_USER_TABLE
MYSQL_LOGIN_FIELD (to email) and MYSQL_WHERE_CLAUSE fields in your
authmysqlrc file.

Note: the spaces between the variable and the value are supposed to be tabs
and not spaces. Also, there should be no trailing spaces or tabs on any of
the lines Courier reads. Courier won't work correctly if you don't make sure
those conditions are met.

##NAME: LOCATION:0
MYSQL_SERVER	localhost
MYSQL_USERNAME	<your-drupal-database-user-name-as-defined-in-your-settings.php-file>
MYSQL_PASSWORD	<your-drupal-database-password-name-as-defined-in-your-settings.php-file>

##NAME: MYSQL_DATABASE:0
MYSQL_DATABASE	<your-drupal-database-name-as-defined-in-your-settings.php-file>

##NAME: MYSQL_USER_TABLE:0
MYSQL_USER_TABLE	couriermta_users

##NAME: MYSQL_CRYPT_PWFIELD:0
MYSQL_CRYPT_PWFIELD	crypt

##NAME: MYSQL_CLEAR_PWFIELD:0
MYSQL_CLEAR_PWFIELD	clear

##NAME: MYSQL_UID_FIELD:0
MYSQL_UID_FIELD	uid

##NAME: MYSQL_GID_FIELD:0
MYSQL_GID_FIELD	gid

##NAME: MYSQL_LOGIN_FIELD:0
MYSQL_LOGIN_FIELD	email

##NAME: MYSQL_HOME_FIELD:0
MYSQL_HOME_FIELD	home

##NAME: MYSQL_NAME_FIELD:0
MYSQL_NAME_FIELD	name

##NAME: MYSQL_MAILDIR_FIELD:0
MYSQL_MAILDIR_FIELD	maildir

##NAME: MYSQL_DEFAULTDELIVERY:0
MYSQL_DEFAULTDELIVERY	defaultdelivery

##NAME: MYSQL_QUOTA_FIELD:0
MYSQL_QUOTA_FIELD	quota

##NAME: MYSQL_AUXOPTIONS:0
MYSQL_AUXOPTIONS_FIELD	auxoptions

##NAME: MYSQL_WHERE_CLAUSE:0
MYSQL_WHERE_CLAUSE	status=1
