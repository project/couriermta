<?php
/**
 * @file
 * This is the code for creating accounts.
 */

/**
 * Create a mailbox account.
 *
 * @param array $edit
 *   The form values submitted by the user.
 * @param object $account
 *   The user object on which the operation is being performed.
 */
function _couriermta_insert_mailbox_account(&$edit, &$account) {
  // Get the data.
  module_load_include('inc', 'couriermta', 'couriermta.common');
  $user_data = _couriermta_create_user_sql_data($edit, $account);

  // Insert the record.
  switch (variable_get('couriermta_password_type', 0)) {
    case 0:
      // Insert encrypted passwords only.
      // @todo: Password encryption needs to be revisited again in the future.
      $crypt = crypt($user_data['password']);
      // $crypt = '{MD5}' . base64_encode(pack('H*', $user_data['password']));
      $clear = '';
      break;

    case 1:
      // Insert cleartext passwords only.
      $crypt = '';
      $clear = $user_data['password'];
      break;

    case 2:
      // Insert both encrypted and cleartext passwords.
      $crypt = crypt($user_data['password']);
      // $crypt = '{MD5}' . base64_encode(pack('H*', $user_data['password']));
      $clear = $user_data['password'];
      break;
  }

  $id = db_insert('couriermta_users')
    ->fields(array(
      'userid' => $user_data['userid'],
      'email' => !empty($user_data['email']) ? $user_data['email'] : '',
      'crypt' => $crypt,
      'clear' => $clear,
      'uid' => $user_data['uid'],
      'gid' => $user_data['gid'],
      'home' => !empty($user_data['home']) ? $user_data['home'] : '',
      'name' => !empty($user_data['name']) ? $user_data['name'] : '',
      'maildir' => !empty($user_data['maildir']) ? $user_data['maildir'] : '',
      'defaultdelivery' => !empty($user_data['defaultdelivery']) ? $user_data['defaultdelivery'] : '',
      'quota' => !empty($user_data['quota']) ? $user_data['quota'] : '',
      'auxoptions' => !empty($user_data['auxoptions']) ? $user_data['auxoptions'] : '',
      'alias' => !empty($user_data['alias']) ? $user_data['alias'] : '',
      'status' => (integer) $user_data['status'],
    ))
    ->execute();

  // Update the insertmailbox file.
  $data = (empty($user_data['maildir'])) ? $user_data['home'] : $user_data['maildir'] . '
';
  _couriermta_update_input_file('insert', 'mailbox', $data);
}


/**
 * Create an alias account.
 *
 * @param array $edit
 *   The form values submitted by the user.
 * @param object $account
 *   The user object on which the operation is being performed.
 */
function _couriermta_insert_aliased_account(&$edit, &$account) {
  // Get the data.
  module_load_include('inc', 'couriermta', 'couriermta.common');
  $user_data = _couriermta_create_user_sql_data($edit, $account);

  // Update the couriermta_users table.
  $id = db_insert('couriermta_users')
    ->fields(array(
      'userid' => $user_data['userid'],
      'alias' => !empty($user_data['alias']) ? $user_data['alias'] : '',
      'status' => (integer) $user_data['status'],
    ))
    ->execute();

  // Update the insertalias file.
  $data = $user_data['email'] . ': ' . $user_data['alias'] . '
';
  _couriermta_update_input_file('insert', 'alias', $data);
}
