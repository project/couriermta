<?php
/**
 * @file
 * This is the code for updating accounts.
 */

/**
 * Update a mailbox account.
 *
 * @param array $edit
 *   The form values submitted by the user.
 * @param object $account
 *   The user object on which the operation is being performed.
 */
function _couriermta_update_mailbox_account(&$edit, &$account) {
  // Get the data.
  module_load_include('inc', 'couriermta', 'couriermta.common');
  $user_data = _couriermta_create_user_sql_data($edit, $account);

  // Get the old mailbox directory before the database changes.
  if ($edit['mail'] != $account->mail) {
    $query = 'SELECT maildir FROM {couriermta_users} WHERE userid = :userid';
    $results = db_query($query, array(':userid' => $account->uid));
    $oldmailboxdir = $results->fetchField();
    if (!$oldmailboxdir) {
      $query = 'SELECT home FROM {couriermta_users} WHERE userid = :userid';
      $results = db_query($query, array(':userid' => $account->uid));
      $oldmailboxdir = $results->fetchField();
    }
  }

  // Update the record.
  if (empty($user_data['password'])) {
    // The data does not include a password.
    $results = db_update('couriermta_users')
      ->fields(array(
        'email' => !empty($user_data['email']) ? $user_data['email'] : '',
        'uid' => $user_data['uid'],
        'gid' => $user_data['gid'],
        'home' => !empty($user_data['home']) ? $user_data['home'] : '',
        'name' => !empty($user_data['name']) ? $user_data['name'] : '',
        'maildir' => !empty($user_data['maildir']) ? $user_data['maildir'] : '',
        'defaultdelivery' => !empty($user_data['defaultdelivery']) ? $user_data['defaultdelivery'] : '',
        'quota' => !empty($user_data['quota']) ? $user_data['quota'] : '',
        'auxoptions' => !empty($user_data['auxoptions']) ? $user_data['auxoptions'] : '',
        'alias' => !empty($user_data['alias']) ? $user_data['alias'] : '',
        'status' => (integer) $user_data['status'],
      ))
      ->condition('userid', $user_data['userid'])
      ->execute();
    if ($results != 1) {
      $message = 'The query inserting data into the couriermta_users table did not succeed.';
      watchdog('couriermta', $message, array(), WATCHDOG_ERROR);
    }
  }
  else {
    // Determine how the password is stored, based on administrative settings.
    switch (variable_get('couriermta_password_type', 0)) {
      case 0:
        // Save encrypted passwords only.
        // @todo: Password encryption needs to be revisited again in the future.
        $crypt_password = crypt($user_data['password']);
        // $crypt_password = '{MD5}' . base64_encode(pack('H*', $user_data['password']));
        $clear_password = '';
        break;

      case 1:
        // Save cleartext passwords only.
        $crypt_password = '';
        $clear_password = $user_data['password'];
        break;

      case 2:
        // Save both encrypted and cleartext passwords.
        $crypt_password = crypt($user_data['password']);
        // $crypt_password = '{MD5}' . base64_encode(pack('H*', $user_data['password']));
        $clear_password = $user_data['password'];
        break;
    }

    // The data includes a password.
    // Check to see if the record already exists.
    $query = 'SELECT userid FROM {couriermta_users} WHERE userid = :userid';
    $results = db_query($query, array(':userid' => $user_data['userid']));
    if ($results->fetchField()) {
      // The record exists.
      $results = db_update('couriermta_users')
        ->fields(array(
          'email' => !empty($user_data['email']) ? $user_data['email'] : '',
          'crypt' => $crypt_password,
          'clear' => $clear_password,
          'uid' => $user_data['uid'],
          'gid' => $user_data['gid'],
          'home' => !empty($user_data['home']) ? $user_data['home'] : '',
          'name' => !empty($user_data['name']) ? $user_data['name'] : '',
          'maildir' => !empty($user_data['maildir']) ? $user_data['maildir'] : '',
          'defaultdelivery' => !empty($user_data['defaultdelivery']) ? $user_data['defaultdelivery'] : '',
          'quota' => !empty($user_data['quota']) ? $user_data['quota'] : '',
          'auxoptions' => !empty($user_data['auxoptions']) ? $user_data['auxoptions'] : '',
          'alias' => !empty($user_data['alias']) ? $user_data['alias'] : '',
          'status' => (integer) !$user_data['status'],
        ))
        ->condition('userid', $user_data['userid'])
        ->execute();
      if ($results != 1) {
        $message = 'The query inserting data into the couriermta_users table did not succeed.';
        watchdog('couriermta', $message, array(), WATCHDOG_ERROR);
      }
    }
    else {
      // The record does not exist, so we need to create it.
      $id = db_insert('couriermta_users')
        ->fields(array(
          'userid' => $user_data['userid'],
          'email' => !empty($user_data['email']) ? $user_data['email'] : '',
          'crypt' => $crypt_password,
          'clear' => $clear_password,
          'uid' => $user_data['uid'],
          'gid' => $user_data['gid'],
          'home' => !empty($user_data['home']) ? $user_data['home'] : '',
          'name' => !empty($user_data['name']) ? $user_data['name'] : '',
          'maildir' => !empty($user_data['maildir']) ? $user_data['maildir'] : '',
          'defaultdelivery' => !empty($user_data['defaultdelivery']) ? $user_data['defaultdelivery'] : '',
          'quota' => !empty($user_data['quota']) ? $user_data['quota'] : '',
          'auxoptions' => !empty($user_data['auxoptions']) ? $user_data['auxoptions'] : '',
          'alias' => !empty($user_data['alias']) ? $user_data['alias'] : '',
          'status' => (integer) $user_data['status'],
        ))
        ->execute();

      // Update the insertmailbox file.
      $data = (empty($user_data['maildir'])) ? $user_data['home'] : $user_data['maildir'] . '
';
      _couriermta_update_input_file('insert', 'mailbox', $data);
    }
  }

  if (!empty($oldmailboxdir)) {
    $query = 'SELECT maildir FROM {couriermta_users} WHERE userid = :userid';
    $results = db_query($query, array(':userid' => $account->uid));
    $newmailboxdir = $results->fetchField();
    if (!$newmailboxdir) {
      $query = 'SELECT home FROM {couriermta_users} WHERE userid = :userid';
      $results = db_query($query, array(':userid' => $account->uid));
      $newmailboxdir = $results->fetchField();
    }

    // Update the updatemailbox file.
    $data = $oldmailboxdir . ':' . $newmailboxdir . '
';
    _couriermta_update_input_file('update', 'mailbox', $data);
  }
}


/**
 * Update an alias account.
 *
 * @param array $edit
 *   The form values submitted by the user.
 * @param object $account
 *   The user object on which the operation is being performed.
 */
function _couriermta_update_aliased_account(&$edit, &$account) {
  // Get the data.
  module_load_include('inc', 'couriermta', 'couriermta.common');
  $user_data = _couriermta_create_user_sql_data($edit, $account);
  $query = 'SELECT alias FROM {couriermta_users} WHERE userid = :userid';
  $results = db_query($query, array(':userid' => $account->uid));
  if ($results->fetchField() != $user_data['alias']) {
    // Update the deletealias file.
    $query2 = 'SELECT alias FROM {couriermta_users} WHERE userid = :userid';
    $results2 = db_query($query2, array(':userid' => $account->uid));
    $data2 = $account->mail . ': ' . $results2->fetchField() . '
';
    _couriermta_update_input_file('delete', 'alias', $data2);
    // Update the insertalias file.
    $data3 = $edit['mail'] . ': ' . $edit['couriermta_email_alias_address'] . '
';
    _couriermta_update_input_file('insert', 'alias', $data3);
  }

  // Update the couriermta_users table.
  $results = db_update('couriermta_users')
    ->fields(array(
      'alias' => $user_data['alias'],
      'status' => (integer) $user_data['status'],
    ))
    ->condition('userid', $user_data['userid'])
    ->execute();
  if ($results != 1) {
    $message = 'The query inserting data into the couriermta_users table did not succeed.';
    watchdog('couriermta', $message, array(), WATCHDOG_ERROR);
  }
}
